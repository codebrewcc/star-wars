﻿(function () {
    'use strict';

    angular
        .module('minima')
        .config(config);

    /** @ngInject */
    function config($httpProvider, $compileProvider) {

        //to use debugger if published call this method in the console: angular.reloadWithDebugInfo();
        $compileProvider.debugInfoEnabled(false);
    }

})();
