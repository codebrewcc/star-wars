'use strict';

angular
    .module('minima', [
        'loadTpls',

        // Load Core Libraries
        'ui.router',
        'oc.lazyLoad',

        //Load App Modules
        //ex. minima.home
    ]);
