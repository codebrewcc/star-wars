(function () {
    'use strict';

    angular
        .module('minima')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider) {

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/');

        $stateProvider.
            // Main Layout Structure
            state('app', {
                abstract: true,
                url: '',
                template: '<ui-view></ui-view>',
            })
            .state('app.home', {
                url: '/',
                templateUrl: 'app/modules/home/home.html',
            })
            .state('app.base', {
                abstract: true,
                url: '/app',
                templateUrl: 'app/modules/shared/tpls/app.html',
                controller: 'AppController as vm',
                resolve: {
                    "minima.shared": function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            [
                                'app_636f7265.js', //loading js for app
                                'vendor_636f7265.js', //loading libraries for app
                                'templates_617070.js', //loading templates for app
                            ]
                        ]);
                    }
                }
            })
            .state('app.base.dashboard', {
                url: '',
                templateUrl: 'app/modules/dashboard/dashboard.html',
                controller: 'DashboardController as vm',
            })
    }
})();
