(function() {
    'use strict';

    angular
        .module('minima')
        .controller('DashboardController', DashboardController);

    function DashboardController(informationService, helpersService, $scope) {

        var vm = this;

        vm.dom = {
            "distance": 1000000,
            "starshipsSearch": "",
            "starships": [],

            //dom functions
            "calculateStops": calculateStops
        };

        function calculateStops(speed, consumables, distance) {
            if (speed !== "unknown" && consumables !== "unknown") {
                return helpersService.calculateStopsRequired(speed, consumables, distance);
            } else {
                return 'N/A';
            }
        };

        $scope.$watch('vm.dom.distance', function(nv) {
            vm.dom.distance = nv.replace(/[^0-9]/g, '');
        });

        vm.init = function() {
            informationService.getStarShips().then(function(res) {
                angular.copy(res, vm.dom.starships);
            });
        }();

    };
})();
