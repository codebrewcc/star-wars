﻿(function () {
    'use strict';

    angular
        .module('minima.shared.factories')
        .factory('enums', enums);

    /** @ngInject */

    function enums() {
        var service = {};

        service.regions = [{ "id": 1, "name": 'EMEA' }, { "id": 2, "name": 'Americas' }, { "id": 3, "name": 'Asia' }];
        return service;
    };

})();
