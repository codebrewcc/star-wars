﻿
(function() {
    'use strict';

    angular
        .module('minima.shared.services')
        .factory('helpersService', helpersService);

    function helpersService() {
        var service = {};

        service.calculateStopsRequired = function(speed, consumables, distance) {
            //seperating unit and quantity for consumables
            var consumablesObj = {
                "unit": consumables.replace(/[0-9]/g, '').trim(),
                "quantity": consumables == "unknown" ? null : parseInt(consumables)
            };

            //deducting how many hours consumables would last
            var consumableLastingHours = moment.duration(moment().add(consumablesObj.quantity, consumablesObj.unit).diff(
                moment()
            ));
            consumableLastingHours = consumableLastingHours.asHours();

            //value is calculated by dividing distance by consumablehours * the speed, assuming speed is always constant
            var returnVal = distance / (consumableLastingHours * parseInt(speed));

            //if the given distance is very small, the result may have a scientific notation, hence value is simply returned as 0 if it is less than 1.
            return (returnVal > 1 ? service.resolveENotation(returnVal) : 0);
        };

        service.resolveENotation = function(x) {
            if (Math.abs(x) < 1.0) {
                var e = parseInt(x.toString().split('e-')[1]);
                if (e) {
                    x *= Math.pow(10, e - 1);
                    x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
                }
            } else {
                var e = parseInt(x.toString().split('+')[1]);
                if (e > 20) {
                    e -= 20;
                    x /= Math.pow(10, e);
                    x += (new Array(e + 1)).join('0');
                }
            }
            return x;
        }

        return service;
    };
})();
