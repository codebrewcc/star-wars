﻿
(function() {
    'use strict';

    angular
        .module('minima.shared.services')
        .factory('informationService', informationService);

    function informationService($http, $q, apiService) {
        var service = {};

        service.getStarShips = function(pageNms) {
            var pageNumbers = pageNms || [1, 2, 3, 4];
            return $q.all(pageNumbers.map(function(pageNumber) {
                return $http({
                    method: 'GET',
                    url: apiService.apiBaseUrl + 'starships/?page=' + pageNumber
                });
            })).then(function(results) {
                var response = [];
                results.forEach(function(res, i) {
                    response = response.concat(res.data.results);
                });
                return response;
            });
        };

        return service;
    };
})();
